﻿using RabbitMQ.Client;
using SenderRabbitMQ;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace RabbitMQtest.RabbitMQ
{
    public class TestRabbitMQ
    {


        public static string SendData()
        {
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@shopfloor-rabbitmq:5672") };
            var factory = new ConnectionFactory() { Uri = new Uri("amqp://schan:insituquality@192.168.137.1:5672") };

            try
            {
                using (var connection = factory.CreateConnection())
                {
                    using (var channel = connection.CreateModel())
                    {
                        channel.ExchangeDeclare(exchange: "insitu",
                            type: ExchangeType.Direct,
                            durable: true,
                            autoDelete: false);


                        List<InputData> testDataList = ReadTestData();
                        foreach (InputData data in testDataList)
                        {
                            byte[] utfBytes = JsonSerializer.SerializeToUtf8Bytes(data);

                            //byte[] utfBytes = Encoding.UTF8.GetBytes(inputJSONString);


                            channel.BasicPublish(exchange: "insitu",
                                                 routingKey: "insitu",
                                                 mandatory: true,
                                                 basicProperties: null,
                                                 body: utfBytes);
                        }

                        return "OK";
                        //Console.WriteLine(" [x] Sent {0}", message);
                    }
                }

            }
            catch (Exception ex)
            {
                return ex.Message;
            }

            //Console.WriteLine(" Press [enter] to exit.");
            //Console.ReadLine();
        }


        public static void DelInsituExchange()
        {
            var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@shopfloor-rabbitmq:5672") };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://schan:insituquality@192.168.137.1:5672") };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDelete("insitu");
                }
            }
        }


        public static void DelDashboardExchange()
        {
            var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@shopfloor-rabbitmq:5672") };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://schan:insituquality@192.168.137.1:5672") };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.ExchangeDelete("dashboard");
                }
            }
        }


        public static void DelInsituQueue()
        {
            var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@shopfloor-rabbitmq:5672") };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://schan:insituquality@192.168.137.1:5672") };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDelete("insitu");
                }
            }
        }

        public static void DelDashboardQueue()
        {
            var factory = new ConnectionFactory() { Uri = new Uri("amqp://guest:guest@shopfloor-rabbitmq:5672") };
            //var factory = new ConnectionFactory() { Uri = new Uri("amqp://schan:insituquality@192.168.137.1:5672") };

            using (var connection = factory.CreateConnection())
            {
                using (var channel = connection.CreateModel())
                {
                    channel.QueueDelete("dashboard");
                }
            }
        }



        public static List<InputData> ReadTestData()
        {
            Task<List<InputData>> t = ReadFile();
            return t.Result;
        }


        private static async Task<List<InputData>> ReadFile()
        {
            using FileStream openStream = File.OpenRead("./testdata_v2.txt");
            List<InputData> testDataList = await JsonSerializer.DeserializeAsync<List<InputData>>(openStream);

            return testDataList;
        }
    
    }
}
