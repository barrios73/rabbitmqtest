﻿using Microsoft.AspNetCore.Mvc;
using RabbitMQtest.RabbitMQ;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RabbitMQtest.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class RabbitController : Controller
    {
        [HttpGet("SendData")]
        public IActionResult SendData()
        {
            string res = TestRabbitMQ.SendData();

            ObjectResult result = new ObjectResult(res);
            result.ContentTypes.Add("application/json");
            result.StatusCode = 200;
            return result;
        }

        [HttpGet("DelInsituExchange")]
        public IActionResult DeleteInsituExchange()
        {
            TestRabbitMQ.DelInsituExchange();
            return NoContent();
        }

        [HttpGet("DelDashboardExchange")]
        public IActionResult DeleteDashboardExchange()
        {
            TestRabbitMQ.DelDashboardExchange();
            return NoContent();
        }


        [HttpGet("DelInsituQueue")]
        public IActionResult DeleteInsituQueue()
        {
            TestRabbitMQ.DelInsituQueue();
            return NoContent();
        }

        [HttpGet("DelDashboardQueue")]
        public IActionResult DeleteDashboardQueue()
        {
            TestRabbitMQ.DelDashboardQueue();
            return NoContent();
        }

    }
}
