﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SenderRabbitMQ
{
    public class InputData
    {
        public string[] InputParamNames { get; set; }
        public double[] Data { get; set; }
        public string MachineId { get; set; }
        public string ProcessId { get; set; }
        public string BatchId { get; set; }
        public string WorkpieceId { get; set; }
        public int ModelType { get; set; }
        public string ModelFile { get; set; }
    }
}
